---
layout: class

# post title
title: "The Zeitgeist Movement Defined, Essay 1: Overview"

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
topicid: intro

excerpt: "Read The Zeitgeist Movement Defined, Essay 1: Overview"


---

At first, we recommend to read the first essay of the book The Zeitgeist Movement Defined. In this text, the core of TZM is presented in a well structured way. We also encourage you to read the whole book!

PDF https://drive.google.com/file/d/0By5SSxpw6TMIR3hjOGRQVy1zUjQ/view