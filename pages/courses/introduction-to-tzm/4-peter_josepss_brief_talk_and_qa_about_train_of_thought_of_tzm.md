---
layout: class

# post title
title: "Peter Joseps's brief talk and Q&A about Train of Thought of TZM"

# post author
author: TZM

# hide post
hidden: false

# class values
type_class: true
topicid: intro

excerpt: ""
video: https://www.youtube.com/embed/24Px_-9rU1A

---

Another great talk of Peter Joseph about The New Human Right Movement:
https://youtu.be/GvkchZADaaA