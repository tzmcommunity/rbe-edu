---
layout: course

#title
title: Introduction to TZM

# thumbnail
thumbnail: /assets/img/rbe-edu-main-logo.png

# wether page and link to it is shown
hidden: false

courseid: "intro"
topicid: "intro"
type_course: true
show_classes: true
order: 2

excerpt: "Introduction to TZM. At first, we recommend to read the first essay of the book The Zeitgeist Movement Defined. In this text, the core of TZM is presented in a well structured way. We also encourage you to read the whole book!"

---

### What

<!--more-->

### Target audience
You.
