---
layout: home
bootstrap: true

# title
postnav_title: "RBE Edu"

# second title
postnav_subtitle: "Learn the train of thought that will change the world."

# second link
postnav_link: "abouttzm"

# second linktext
postnav_linktext: "Learn more"

# home page header image
header_image: "/assets/img/autumn-219972_1280.jpg"

---
